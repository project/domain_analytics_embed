<?php

namespace Drupal\domain_analytics_embed;

use Drupal\Core\Form\FormStateInterface;
use Drupal\domain\Form\DomainForm as OriginalDomainForm;
use Drupal\domain\Entity\Domain;

class DomainForm extends OriginalDomainForm
{
    public function form(array $form, FormStateInterface $form_state): array
    {
        $form = parent::form($form, $form_state);

        /** @var Domain $domain */
        $domain = $this->entity;

        $form['ga_code_type'] = [
            '#type'          => 'radios',
            '#title'         => $this->t('Code type'),
            '#options'       => [
                'gtm' => $this->t('GTM (Tagmanager)'),
                'ua'  => $this->t('UA (Universal Analytics)'),
                'g'   => $this->t('GA4 (Google Analytics 4-property)'),
            ],
            '#default_value' => $domain->getThirdPartySetting('domain_analytics_embed', 'ga_code_type'),
        ];

        $form['ga_code'] = [
            '#type'          => 'textfield',
            '#title'         => t('Google Tagmanager / UA Analytics code / Google Analytics 4-property'),
            '#field_prefix'  => 'GTM-/UA-/G-',
            '#description'   => t('fill in the GTM-XXXXXXX / UA-XXXXXXX / G-XXXXXXX code without the prefix (GTM, UA, G).'),
            '#default_value' => $domain->getThirdPartySetting('domain_analytics_embed', 'ga_code'),
        ];

        return $form;
    }

    public function save(array $form, FormStateInterface $form_state): void
    {
        /** @var Domain $domain */
        $domain = $this->entity;

        $domain->setThirdPartySetting('domain_analytics_embed', 'ga_code', $form_state->getValue('ga_code'));
        $domain->setThirdPartySetting('domain_analytics_embed', 'ga_code_type', $form_state->getValue('ga_code_type'));

        parent::save($form, $form_state);
    }
}