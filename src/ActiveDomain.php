<?php

namespace Drupal\domain_analytics_embed;

class ActiveDomain
{
    public function getGaTagPartOne(): array
    {
        $gaCode = $this->getGaCode();
        $gaType = $this->getGaType();

        if (empty($gaCode) or empty($gaType)) {
            return [];
        }

        if ($gaType === 'gtm') {
            return [
                'type'    => $gaType,
                'scripts' => [
                    'script' => "<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                                })(window,document,'script','dataLayer','GTM-" . $gaCode . "');</script>",
                ],
            ];
        } else {
            return [
                'type'    => $gaType,
                'scripts' => [
                    'attributes' => [
                        'src'   => "https://www.googletagmanager.com/gtag/js?id=" . strtoupper($gaType) . "-" . $gaCode,
                        'async' => '',
                    ],
                    'script'     => "<script>window.dataLayer = window.dataLayer || [];
                              function gtag(){dataLayer.push(arguments);}
                              gtag('js', new Date());
                              gtag('config', '" . strtoupper($gaType) . "-" . $gaCode . "');</script>",
                ],
            ];
        }
    }

    public function getGaTagPartTwo(): string
    {
        $gaCode = $this->getGaCode();
        $gaType = $this->getGaType();
    
        if (empty($gaCode) or $gaType === 'ua' or $gaType === 'g') {
            return '';
        }

        return '<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-' . $gaCode . '"
                height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>';
    }

    private function getGaCode(): string
    {
        $gaCode = $this->getActiveDomainFields();

        if (!isset($gaCode['ga_code'])) {
            return '';
        }

        return $gaCode['ga_code'];
    }

    private function getGaType(): string
    {
        $gaCode = $this->getActiveDomainFields();

        if (!isset($gaCode['ga_code_type'])) {
            return '';
        }

        return $gaCode['ga_code_type'];
    }

    private function getActiveDomainFields(): array
    {
        return $this->getActiveDomain()->getThirdPartySettings('domain_analytics_embed');
    }

    private function getActiveDomain()
    {
        return \Drupal::service('domain.negotiator')->getActiveDomain();
    }
}